using System;

namespace Mesozoic
{
    public class Dinosaur
    {
        public string name;
        public string specie;
        public int age;


        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            this.age = age;
        }

        public string sayHello()
        {
            return String.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie, this.age);
        }

        public string roar()
        {
            return("Grrr");
        }
    }
}